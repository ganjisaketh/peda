set auto-load safe-path /

set history save
set verbose off
set print pretty on
set print array off
set print array-indexes on
set python print-stack full

# Uncomment the following line if you want to use PEDA
# (https://github.com/longld/peda).
#source ~/.cs4348/peda.py

# If PEDA is enabled above, you must comment out the following two lines
# to disable GDB dashboard (https://github.com/cyrus-and/gdb-dashboard).
# Only one of them can be used at a time.
source ~/.cs4348/gdb-dashboard.py
python Dashboard.start()
